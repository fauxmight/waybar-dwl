# waybar-dwl.sh

![waybar-dwl.sh screenshot](/resources/waybar-dwl-example.png "waybar-dwl.sh in action")

waybar-dwl.sh generates [dwl](https://github.com/djpohly/dwl) tag, layout and title info for [waybar](https://github.com/Alexays/Waybar)
----------------------------------------------------------------------------------------------------------------------------------------

waybar-dwl.sh was originally based upon [dwl-tags.sh](https://codeberg.org/novakane/yambar/src/branch/master/examples/scripts/dwl-tags.sh) by user "novakane" (Hugo Machet) that does same job for [yambar](https://codeberg.org/dnkl/yambar)



REQUIREMENTS:
 - inotifywait ( 'inotify-tools' on arch )
 - Launch dwl with `dwl > ~.cache/dwltags` or change `$dwl_output_filename`
 - Modify "labels" array with your choice and number of labels
 - See waybar-dwl.sh comments for usage details
